# CRUD Operations for Customers using Nodejs and MySQL

## Pre-requisites:

-   Mysql ~ `sudo apt install mysql-server -y`
-   NodeJs ~ `sudo apt install nodejs -y` & `sudo apt install npm -y`

## Installation

#### Setup Database:
1. Connecting to the MySQL Server:
	
	`sudo mysql -u root`
	
2.  Change the root password:
	
	`ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password by 'Testpassword@123';`
	
3. Create a new database:
    
	`CREATE DATABASE customersdb;`
    
4. Use the named database as the default:
	
	`use customersdb;`
	
5. Create a table:
	```
	CREATE TABLE customer (
	  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	  name VARCHAR(50) NOT NULL,
	  address VARCHAR(100) NOT NULL,
	  phone VARCHAR(15)
	);
	```

#### Running Locally:

1.  Create a clone of the crud-operation-nodejs-mysql repository on your local machine:
	
	`git clone https://gitlab.com/Johnson-Dsouza/crud-operation-nodejs-mysql.git`

2. To Install all the dependencies with:
	
	`cd crud-operation-nodejs-mysql`
	
	`npm install`

3. To run application:
	
	`npm start `
	
4. Browse to  [http://localhost:3000](http://localhost:3000/)
